#![feature(core)]
#![feature(io)]

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate num;
extern crate rand;
extern crate byteorder;
extern crate sdl2;

use std::path::Path;
use sdl2::SdlResult;
use sdl2::video;
use sdl2::video::{Window, WindowPos};
use sdl2::render;
use sdl2::render::{Renderer, RenderDrawer, Texture, TextureAccess, RenderDriverIndex};
use sdl2::pixels::PixelFormatEnum;
use sdl2::timer;
use sdl2::event::Event;
use sdl2::keycode::KeyCode;
use chip16::{SCREEN_WIDTH, SCREEN_HEIGHT, Chip16, Controller};

mod chip16;


const FRAME_TICKS: u32 = 16;


struct FrontEnd<'a> {
    drawer: RenderDrawer<'a>,
    frame_buffer: Box<[u8; (4 * SCREEN_WIDTH * SCREEN_HEIGHT) as usize]>,
    frame_texture: Texture<'a>
}

impl<'a> FrontEnd<'a> {
    fn new(renderer: &'a Renderer) -> FrontEnd<'a> {
        match renderer.create_texture(PixelFormatEnum::ARGB8888,
                                      TextureAccess::Streaming,
                                      (SCREEN_WIDTH, SCREEN_HEIGHT)) {
            Ok(texture) => {
                FrontEnd {
                    drawer: renderer.drawer(),
                    frame_buffer: Box::new([0; (4 * SCREEN_WIDTH * SCREEN_HEIGHT) as usize]),
                    frame_texture: texture
                }
            }
            Err(error) => panic!(error)
        }
    }

    fn draw(&mut self, chip16: &Chip16) -> SdlResult<()> {
        let (bg_r, bg_g, bg_b) = chip16.gfx_regs.palette[chip16.gfx_regs.bg as usize];
        for y in 0..SCREEN_HEIGHT {
            for x in 0..SCREEN_WIDTH {
                let index = chip16.video_mem[(x + y * SCREEN_WIDTH) as usize];
                if index != 0 {
                    let (r, g, b) = chip16.gfx_regs.palette[index as usize];
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 0) as usize] = b;
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 1) as usize] = g;
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 2) as usize] = r;
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 3) as usize] = 255;
                } else {
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 0) as usize] = bg_b;
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 1) as usize] = bg_g;
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 2) as usize] = bg_r;
                    self.frame_buffer[(4 * (x + y * SCREEN_WIDTH) + 3) as usize] = 255;
                }
            }
        }

        try!(self.frame_texture.update(None, &self.frame_buffer[..], 4 * SCREEN_WIDTH));
        self.drawer.copy(&self.frame_texture, None, None);
        self.drawer.present();
        Ok(())
    }
}


fn main() {
    env_logger::init().unwrap();

    let mut args = std::env::args();
    let filename = if args.len() == 2 {
        args.nth(1).expect("blub")
    } else {
        println!("usage: chip16 <file>");
        return;
    };
    let mut chip16 = Chip16::new();
    chip16.load_rom(&Path::new(&filename[..])).unwrap();

    let sdl_context = sdl2::init(sdl2::INIT_VIDEO).unwrap();

    let window = Window::new("chip16", WindowPos::PosCentered, WindowPos::PosCentered,
                             320, 240, video::OPENGL).unwrap();

    let renderer = Renderer::from_window(window, RenderDriverIndex::Auto,
                                         render::ACCELERATED).unwrap();

    let mut front_end = FrontEnd::new(&renderer);

    let mut controller1 = Controller::new();

    let mut last_frame_end = timer::get_ticks();
    'main: loop {
        let _ = front_end.draw(&chip16);
        chip16.mem.mapped_io.controller1 = controller1;
        chip16.vblank();
        'cpu: for _ in 0..16667 {
            chip16.step();
            if chip16.waiting_for_vblank() {
                break 'cpu;
            }
        }

        let mut ticks = timer::get_ticks();
        while ticks - last_frame_end < FRAME_TICKS {
            timer::delay(1);
            ticks = timer::get_ticks();
        }
        last_frame_end = ticks;

        let mut event_pump = sdl_context.event_pump();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} => break 'main,
                Event::KeyDown { keycode, .. } => {
                    match keycode {
                        KeyCode::Up     => controller1.up = true,
                        KeyCode::Down   => controller1.down = true,
                        KeyCode::Left   => controller1.left = true,
                        KeyCode::Right  => controller1.right = true,
                        KeyCode::RShift => controller1.select = true,
                        KeyCode::Return => controller1.start = true,
                        KeyCode::A      => controller1.a = true,
                        KeyCode::S      => controller1.b = true,
                        KeyCode::Escape => break 'main,
                        _ => ()
                    }
                }
                Event::KeyUp { keycode, .. } => {
                    match keycode {
                        KeyCode::Up     => controller1.up = false,
                        KeyCode::Down   => controller1.down = false,
                        KeyCode::Left   => controller1.left = false,
                        KeyCode::Right  => controller1.right = false,
                        KeyCode::RShift => controller1.select = false,
                        KeyCode::Return => controller1.start = false,
                        KeyCode::A      => controller1.a = false,
                        KeyCode::S      => controller1.b = false,
                        _ => ()
                    }
                }
                _ => ()
            }
        }
    }
}
