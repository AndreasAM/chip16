use std::path::AsPath;
use std::io::{self, Read, Seek, SeekFrom};
use std::fs::File;
use std::slice::bytes;
use std::slice::bytes::MutableByteVector;
use std::num::FromPrimitive;
use num::Integer;
use rand::{weak_rng, Rng, XorShiftRng};
use byteorder::{LittleEndian, ReadBytesExt};


pub const MAGIC_NUMBER: u32 = 0x36314843; // "CH16"

pub const SCREEN_WIDTH: i32 = 320;
pub const SCREEN_HEIGHT: i32 = 240;
pub const PALETTE_SIZE: i32 = 16;
pub static PALETTE: [(u8, u8, u8); PALETTE_SIZE as usize] = [
    (0x00, 0x00, 0x00), (0x00, 0x00, 0x00),
    (0x88, 0x88, 0x88), (0xBF, 0x39, 0x32),
    (0xDE, 0x7A, 0xAE), (0x4C, 0x3D, 0x21),
    (0x90, 0x5F, 0x25), (0xE4, 0x94, 0x52),
    (0xEA, 0xD9, 0x79), (0x53, 0x7A, 0x3B),
    (0xAB, 0xD5, 0x4A), (0x25, 0x2E, 0x38),
    (0x00, 0x46, 0x7F), (0x68, 0xAB, 0xCC),
    (0xBC, 0xDE, 0xE4), (0xFF, 0xFF, 0xFF)];

pub const STACK_START: u16 = 0xfdf0;
pub const MAPPED_IO_START: u16 = 0xfff0;

const C_MASK: u8 = 0x02;
const Z_MASK: u8 = 0x04;
const O_MASK: u8 = 0x40;
const N_MASK: u8 = 0x80;


pub trait ReadMem {
    fn read_byte(&self, address: u16) -> u8;
    fn read_word(&self, address: u16) -> u16;
}

pub trait WriteMem {
    fn write_word(&mut self, address: u16, word: u16);
}


pub struct Flags(u8);

impl Flags {
    pub fn new() -> Flags {
        Flags(0)
    }

    pub fn carry(&self) -> bool {
        let Flags(flags) = *self;
        flags & C_MASK != 0
    }

    pub fn zero(&self) -> bool {
        let Flags(flags) = *self;
        flags & Z_MASK != 0
    }

    pub fn overflow(&self) -> bool {
        let Flags(flags) = *self;
        flags & O_MASK != 0
    }

    pub fn negative(&self) -> bool {
        let Flags(flags) = *self;
        flags & N_MASK != 0
    }

    pub fn set_carry(&mut self, carry: bool) {
        let Flags(ref mut flags) = *self;
        if carry {
            *flags = *flags | C_MASK;
        } else {
            *flags = *flags & !C_MASK;
        }
    }

    pub fn set_zero(&mut self, zero: bool) {
        let Flags(ref mut flags) = *self;
        if zero {
            *flags = *flags | Z_MASK;
        } else {
            *flags = *flags & !Z_MASK;
        }
    }

    pub fn set_overflow(&mut self, overflow: bool) {
        let Flags(ref mut flags) = *self;
        if overflow {
            *flags = *flags | O_MASK;
        } else {
            *flags = *flags & !O_MASK;
        }
    }

    pub fn set_negative(&mut self, negative: bool) {
        let Flags(ref mut flags) = *self;
        if negative {
            *flags = *flags | N_MASK;
        } else {
            *flags = *flags & !N_MASK;
        }
    }
}


pub struct GfxRegisters {
    pub bg: u8,
    pub spritew: u8,
    pub spriteh: u8,
    pub hflip: bool,
    pub vflip: bool,
    pub palette: [(u8, u8, u8); PALETTE_SIZE as usize]
}

impl GfxRegisters {
    pub fn new() -> GfxRegisters {
        GfxRegisters {
            bg: 0,
            spritew: 0,
            spriteh: 0,
            hflip: false,
            vflip: false,
            palette: PALETTE
        }
    }
}


#[derive(Copy)]
pub struct Controller {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
    pub select: bool,
    pub start: bool,
    pub a: bool,
    pub b: bool
}

impl Controller {
    pub fn new() -> Controller {
        Controller {
            up: false,
            down: false,
            left: false,
            right: false,
            select: false,
            start: false,
            a: false,
            b: false
        }
    }
}

impl ReadMem for Controller {
    fn read_byte(&self, address: u16) -> u8 {
        if address == 0 {
            self.up as u8
            | ((self.down as u8) << 1)
            | ((self.left as u8) << 2)
            | ((self.right as u8) << 3)
            | ((self.select as u8) << 4)
            | ((self.start as u8) << 5)
            | ((self.a as u8) << 6)
            | ((self.b as u8) << 7)
        } else {
            0
        }
    }

    fn read_word(&self, address: u16) -> u16 {
        if address == 0 {
            self.up as u16
            | ((self.down as u16) << 1)
            | ((self.left as u16) << 2)
            | ((self.right as u16) << 3)
            | ((self.select as u16) << 4)
            | ((self.start as u16) << 5)
            | ((self.a as u16) << 6)
            | ((self.b as u16) << 7)
        } else {
            0
        }
    }
}

pub struct MappedIO {
    pub controller1: Controller,
    pub controller2: Controller
}

impl MappedIO {
    pub fn new() -> MappedIO {
        MappedIO {
            controller1: Controller::new(),
            controller2: Controller::new()
        }
    }
}

impl ReadMem for MappedIO {
    fn read_byte(&self, address: u16) -> u8 {
        if address < 2 {
            self.controller1.read_byte(address)
        } else if address < 4 {
            self.controller2.read_byte(address - 2)
        } else {
            0
        }
    }

    fn read_word(&self, address: u16) -> u16 {
        if address < 2 {
            self.controller1.read_word(address)
        } else if address < 4 {
            self.controller2.read_word(address - 2)
        } else {
            0
        }
    }
}

pub struct Memory {
    pub ram: [u8; MAPPED_IO_START as usize],
    pub mapped_io: MappedIO
}

impl Memory {
    pub fn new() -> Memory {
        Memory {
            ram: [0; MAPPED_IO_START as usize],
            mapped_io: MappedIO::new()
        }
    }
}

impl ReadMem for Memory {
    fn read_byte(&self, address: u16) -> u8 {
        if address < MAPPED_IO_START {
            self.ram[address as usize]
        } else {
            self.mapped_io.read_byte(address - MAPPED_IO_START)
        }
    }

    fn read_word(&self, address: u16) -> u16 {
        if address + 1 < MAPPED_IO_START {
            self.ram[address as usize] as u16
            | ((self.ram[address as usize + 1] as u16) << 8)
        } else {
            self.mapped_io.read_word(address - MAPPED_IO_START)
        }
    }
}

impl WriteMem for Memory {
    fn write_word(&mut self, address: u16, word: u16) {
        if address < MAPPED_IO_START {
            self.ram[address as usize] = word as u8;
            self.ram[address as usize + 1] = (word >> 8) as u8;
        } else {
            panic!("write to mapped io")
        }
    }
}


#[derive(FromPrimitive, Debug)]
enum Condition {
    Z   = 0,
    NZ  = 1,
    N   = 2,
    NN  = 3,
    P   = 4,
    O   = 5,
    NO  = 6,
    A   = 7,
    AE  = 8,
    B   = 9,
    BE  = 10,
    G   = 11,
    GE  = 12,
    L   = 13,
    LE  = 14,
    RES = 15
}

// impl fmt::Debug for Condition {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         f.write(match *self {
//             Condition::Z   => "Z",
//             Condition::NZ  => "NZ",
//             Condition::N   => "N",
//             Condition::NN  => "NN",
//             Condition::P   => "P",
//             Condition::O   => "O",
//             Condition::NO  => "NO",
//             Condition::A   => "A",
//             Condition::AE  => "AE",
//             Condition::B   => "B",
//             Condition::BE  => "BE",
//             Condition::G   => "G",
//             Condition::GE  => "GE",
//             Condition::L   => "L",
//             Condition::LE  => "LE",
//             Condition::RES => "RES"
//         }.as_bytes())
//     }
// }

fn nibbles_from_yx(yx: u8) -> (usize, usize) {
    ((yx & 0xf) as usize, ((yx >> 4) & 0xf) as usize)
}


pub struct Chip16 {
    pc: u16,
    sp: u16,
    r: [i16; 16],
    flags: Flags,

    pub mem: Memory,
    pub gfx_regs: GfxRegisters,
    pub video_mem: [u8; (SCREEN_WIDTH * SCREEN_HEIGHT) as usize],
    waiting_for_vblank: bool,
    rng: XorShiftRng
}

impl Chip16 {
    pub fn new() -> Chip16 {
        Chip16 {
            pc: 0,
            sp: STACK_START,
            r: [0; 16],
            flags: Flags::new(),

            mem: Memory::new(),
            gfx_regs: GfxRegisters::new(),
            video_mem: [0; (SCREEN_WIDTH * SCREEN_HEIGHT) as usize],
            waiting_for_vblank: false,
            rng: weak_rng()
        }
    }

    pub fn load_rom<P: AsPath>(&mut self, path: P) -> Result<(), io::Error> {
        let mut file = try!(File::open(path));

        let magic_number = try!(file.read_u32::<LittleEndian>());
        let mut size = None;
        let mut start_address = 0;
        if magic_number == MAGIC_NUMBER {
            let mut _reserved = [0];
            try!(file.read(&mut _reserved));
            let mut _version = [0];
            try!(file.read(&mut _version));
            size = Some(try!(file.read_u32::<LittleEndian>()) as usize);
            start_address = try!(file.read_u16::<LittleEndian>());
            let _checksum = file.read_u32::<LittleEndian>();
        } else {
            try!(file.seek(SeekFrom::Start(0)));
        }

        let mut rom = vec![];
        try!(file.read_to_end(&mut rom));

        let size = match size {
            Some(size) => {
                if rom.len() < size {
                    panic!("unexpected EOF: {:x}, {:x}", size, rom.len());
                } else {
                    size
                }
            }
            None => rom.len()
        };

        debug!("ROM length: {}", rom.len());
        debug!("RAM length: {}", self.mem.ram.len());
        bytes::copy_memory(&mut self.mem.ram, &rom[..size]);
        self.pc = start_address;
        Ok(())
    }

    pub fn waiting_for_vblank(&self) -> bool {
        self.waiting_for_vblank
    }

    pub fn vblank(&mut self) {
        self.waiting_for_vblank = false;
    }

    fn draw(&mut self, sprite_x: i16, sprite_y: i16, sprite_start: u16) {
        let (start_x, dx) = match self.gfx_regs.hflip {
            false => (sprite_x as i32, 1),
            true => (sprite_x as i32 + 2 * self.gfx_regs.spritew as i32, -1)
        };
        let (start_y, dy) = match self.gfx_regs.vflip {
            false => (sprite_y as i32, 1),
            true => (sprite_y as i32 + self.gfx_regs.spriteh as i32, -1)
        };
        self.flags.set_carry(false);
        for y in 0..self.gfx_regs.spriteh as i32 {
            for b in 0..self.gfx_regs.spritew as i32 {
                let byte = self.mem.read_byte(sprite_start + (b + y * self.gfx_regs.spritew as i32) as u16);
                let index = start_x + dx * 2 * b
                          + (start_y + dy * y) * SCREEN_WIDTH as i32;
                let nibble = (byte >> 4) & 0xf;
                if nibble != 0 && is_in_bounds(start_x + dx * 2 * b, start_y + dy * y) {
                    if !self.flags.carry() {
                        self.flags.set_carry(self.video_mem[index as usize] != 0);
                    }
                    self.video_mem[index as usize] = nibble;
                }
                let nibble = byte & 0xf;
                if nibble != 0 && is_in_bounds(start_x + dx * 2 * b + 1, start_y + dy * y) {
                    if !self.flags.carry() {
                        self.flags.set_carry(self.video_mem[(index + dx) as usize] != 0);
                    }
                    self.video_mem[(index + dx) as usize] = nibble;
                }
            }
        }

        fn is_in_bounds(x: i32, y: i32) -> bool {
            x >= 0 && x < SCREEN_WIDTH as i32
                && y >= 0 && y < SCREEN_HEIGHT as i32
        }
    }

    fn check_condition(&self, cond: Condition) -> bool {
        match cond {
            Condition::Z => self.flags.zero(),
            Condition::NZ => !self.flags.zero(),
            Condition::N => self.flags.negative(),
            Condition::NN => !self.flags.negative(),
            Condition::P => !self.flags.negative() && !self.flags.zero(),
            Condition::O => self.flags.overflow(),
            Condition::NO => !self.flags.overflow(),
            Condition::A => !self.flags.carry() && !self.flags.zero(),
            Condition::AE => !self.flags.carry(),
            Condition::B => self.flags.carry(),
            Condition::BE => self.flags.carry() || self.flags.zero(),
            Condition::G =>
                (self.flags.overflow() == self.flags.negative()) && !self.flags.zero(),
            Condition::GE => self.flags.overflow() == self.flags.negative(),
            Condition::L => self.flags.overflow() != self.flags.negative(),
            Condition::LE => 
                (self.flags.overflow() != self.flags.negative()) || self.flags.zero(),
            Condition::RES => panic!("reserved conditional")
        }
    }

    fn add(&mut self, x: i16, y: i16) -> i16 {
        // the two sequential casts prevent sign extending
        let r: u32 = x as u16 as u32 + y as u16 as u32;
        self.flags.set_carry(r > 0xffff);
        self.flags.set_zero(r == 0);
        self.flags.set_overflow(((r as i16 & !x & !y) | (!r as i16 & x & y)) < 0);
        self.flags.set_negative((r as i16) < 0);
        r as i16
    }

    fn sub(&mut self, x: i16, y: i16) -> i16 {
        // the two sequential casts prevent sign extending
        let r = x as u16 as i32 - y as u16 as i32;
        self.flags.set_carry(r as u32 > 0xffff);
        self.flags.set_zero(r == 0);
        self.flags.set_overflow(((!r as i16 & x & !y) | (r as i16 & !x & y)) < 0);
        self.flags.set_negative((r as i16) < 0);
        r as i16
    }

    fn and(&mut self, x: i16, y: i16) -> i16 {
        let r = x & y;
        self.flags.set_carry(false);
        self.flags.set_zero(r == 0);
        self.flags.set_overflow(false);
        self.flags.set_negative(r < 0);
        r
    }

    fn or(&mut self, x: i16, y: i16) -> i16 {
        let r = x | y;
        self.flags.set_carry(false);
        self.flags.set_zero(r == 0);
        self.flags.set_overflow(false);
        self.flags.set_negative(r < 0);
        r
    }

    fn xor(&mut self, x: i16, y: i16) -> i16 {
        let r = x ^ y;
        self.flags.set_carry(false);
        self.flags.set_zero(r == 0);
        self.flags.set_overflow(false);
        self.flags.set_negative(r < 0);
        r
    }

    fn mul(&mut self, x: i16, y: i16) -> i16 {
        // the two sequential casts prevent sign extending
        let r = x as u16 as u32 * y as u16 as u32;
        self.flags.set_carry(r > 0xffff);
        self.flags.set_zero(r == 0);
        self.flags.set_overflow(r as i16 ^ x ^ y < 0);
        self.flags.set_negative((r as i16) < 0);
        r as i16
    }

    fn div(&mut self, x: i16, y: i16) -> i16 {
        let (div, rem) = x.div_rem(&y);
        self.flags.set_carry(rem != 0);
        self.flags.set_zero(div == 0);
        self.flags.set_overflow(false);
        self.flags.set_negative(div < 0);
        div
    }

    fn modulo(&mut self, x: i16, y: i16) -> i16 {
        let modulo = x.mod_floor(&y);
        self.flags.set_carry(false);
        self.flags.set_zero(modulo == 0);
        self.flags.set_overflow(false);
        self.flags.set_negative(modulo < 0);
        modulo
    }

    fn rem(&mut self, x: i16, y: i16) -> i16 {
        let (_, rem) = x.div_rem(&y);
        self.flags.set_carry(false);
        self.flags.set_zero(rem == 0);
        self.flags.set_overflow(false);
        self.flags.set_negative(rem < 0);
        rem
    }

    fn shl(&mut self, x: i16, n: u8) -> i16 {
        if n < 16 {
            let r = x << n;
            self.flags.set_carry(false);
            self.flags.set_zero(r == 0);
            self.flags.set_overflow(false);
            self.flags.set_negative(r < 0);
            r
        } else {
            self.flags.set_carry(false);
            self.flags.set_zero(true);
            self.flags.set_overflow(false);
            self.flags.set_negative(false);
            0
        }
    }

    fn shr(&mut self, x: i16, n: u8) -> i16 {
        if n < 16 {
            let r = (x as u16 >> n) as i16;
            assert!(n == 0 || r >= 0);
            self.flags.set_carry(false);
            self.flags.set_zero(r == 0);
            self.flags.set_overflow(false);
            self.flags.set_negative(r < 0);
            r
        } else {
            self.flags.set_carry(false);
            self.flags.set_zero(true);
            self.flags.set_overflow(false);
            self.flags.set_negative(false);
            0
        }
    }

    fn sar(&mut self, x: i16, n: u8) -> i16 {
        if n < 16 {
            let r = x >> n;
            assert!((x < 0) == (r < 0));
            self.flags.set_carry(false);
            self.flags.set_zero(r == 0);
            self.flags.set_overflow(false);
            self.flags.set_negative(r < 0);
            r
        } else {
            self.flags.set_carry(false);
            self.flags.set_zero(x >= 0);
            self.flags.set_overflow(false);
            self.flags.set_negative(x < 0);
            if x < 0 {
                -1
            } else {
                0
            }
        }
    }

    fn push(&mut self, word: u16) {
        self.mem.write_word(self.sp, word);
        self.sp += 2;
    }

    fn pop(&mut self) -> u16 {
        self.sp -= 2;
        self.mem.read_word(self.sp)
    }

    fn load_palette(&mut self, address: u16) {
        for i in 0u16..16 {
            let r = self.mem.read_byte(address + 3 * i);
            let g = self.mem.read_byte(address + 3 * i + 1);
            let b = self.mem.read_byte(address + 3 * i + 2);
            self.gfx_regs.palette[i as usize] = (r, g, b);
        }
    }

    pub fn step(&mut self) {
        debug!("");
        debug!("    PC={:04x}  SP={:04x}  C={}  Z={}  O={}  N={}\n    \
               R0={:6}  R1={:6}  R2={:6}  R3={:6}  \
               R4={:6}  R5={:6}  R6={:6}  R7={:6}\n    \
               R8={:6}  R9={:6}  RA={:6}  RB={:6}  \
               RC={:6}  RD={:6}  RE={:6}  RF={:6}",
               self.pc, self.sp,
               self.flags.carry() as u8, self.flags.zero() as u8,
               self.flags.overflow() as u8, self.flags.negative() as u8,
               self.r[0] as u16, self.r[1] as u16, self.r[2] as u16, self.r[3] as u16,
               self.r[4] as u16, self.r[5] as u16, self.r[6] as u16, self.r[7] as u16,
               self.r[8] as u16, self.r[9] as u16, self.r[10] as u16, self.r[11] as u16,
               self.r[12] as u16, self.r[13] as u16, self.r[14] as u16, self.r[15] as u16);
        let word1 = self.mem.read_word(self.pc);
        let word2 = self.mem.read_word(self.pc+2);
        let op = [word1 as u8, (word1 >> 8) as u8, word2 as u8, (word2 >> 8) as u8];
        self.pc += 4;
        match op {
            [0x00, 0, 0, 0] => (), // NOP
            [0x01, 0, 0, 0] => { // CLS
                debug!("CLS");
                self.gfx_regs.bg = 0;
                self.video_mem.set_memory(0);
            }
            [0x02, 0, 0, 0] => { // VBLNK
                debug!("VBLNK");
                self.waiting_for_vblank = true;
            }
            [0x03, 0, n, 0] if n < 16 => { // BGC N
                debug!("BGC {:x}", n);
                self.gfx_regs.bg = n;
            }
            [0x04, 0, l, h] => { // SPR HHLL
                debug!("SPR {:02x}{:02x}", h, l);
                self.gfx_regs.spritew = l;
                self.gfx_regs.spriteh = h;
            }
            [0x05, yx, l, h] => { // DRW RX, RY, HHLL
                let (x, y) = nibbles_from_yx(yx);
                let word = ((h as u16) << 8) | l as u16;
                debug!("DRW R{:X}={:04x}, R{:X}={:04x}, {:02x}{:02x}",
                       x, self.r[x], y, self.r[y], h, l);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.draw(tx, ty, word);
            }
            [0x06, yx, z, 0] if z < 16 => { // DRW RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("DRW R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty, tz) = (self.r[x], self.r[y], self.r[z]);
                self.draw(tx, ty, tz as u16);
            }
            [0x07, x, l, h] if x < 16 => { // RND RX, HHLL
                let x = x as usize;
                debug!("RND R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                // word is u32 to be able to store 0xffff + 1;
                let word = ((h as u32) << 8) | l as u32;
                self.r[x] = self.rng.gen_range(0, word + 1) as i16;
            }
            [0x08, 0, 0, hv] if hv < 4 => { // FLIP H, V
                let hflip = (hv >> 1) & 1;
                let vflip = hv & 1;
                debug!("FLIP {}{}", hflip, vflip);
                self.gfx_regs.hflip = hflip != 0;
                self.gfx_regs.vflip = vflip != 0;
            }
            [0x09, 0, 0, 0] => { // SND0
                debug!("SND0");
                // TODO
            }
            [0x0a, 0, l, h] => { // SND1 HHLL
                debug!("SND1 {:02x}{:02x}", h, l);
                // TODO
            }
            [0x0b, 0, l, h] => { // SND2 HHLL
                debug!("SND2 {:02x}{:02x}", h, l);
                // TODO
            }
            [0x0c, 0, l, h] => { // SND3 HHLL
                debug!("SND3 {:02x}{:02x}", h, l);
                // TODO
            }
            [0x0d, x, l, h] if x < 16 => { // SNP RX, HHLL
                let x = x as usize;
                debug!("SNP R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                // TODO
            }
            [0x0e, ad, sr, vt] => { // SNG AD, VTSR
                debug!("SNG {:02x}, {:02x}{:02x}", ad, vt, sr);
                // TODO
            }
            [0x10, 0, l, h] => { // JMP HHLL
                debug!("JMP {:02x}{:02x}", h, l);
                let word = ((h as u16) << 8) | l as u16;
                self.pc = word;
            }
            [0x11, 0, l, h] => { // JMC HHLL
                debug!("JMC {:02x}{:02x}", h, l);
                if self.flags.carry() {
                    let word = ((h as u16) << 8) | l as u16;
                    self.pc = word;
                }
            }
            [0x12, x, l, h] if x < 16 => { // Jx HHLL
                let condition = FromPrimitive::from_u8(x).unwrap();
                debug!("J{:?} {:02x}{:02x}", condition, h, l);
                if self.check_condition(condition) {
                    let word = ((h as u16) << 8) | l as u16;
                    self.pc = word;
                }
            }
            [0x13, yx, l, h] => { // JME RX, RY, HHLL
                let (x, y) = nibbles_from_yx(yx);
                let word = ((h as u16) << 8) | l as u16;
                debug!("JME R{:X}={:04x}, R{:X}={:04x}, {:02x}{:02x}",
                       x, self.r[x], y, self.r[y], h, l);
                if self.r[x] == self.r[y] {
                    self.pc = word;
                }
            }
            [0x14, 0, l, h] => { // CALL HHLL
                debug!("CALL {:02x}{:02x}", h, l);
                let pc = self.pc;
                let word = ((h as u16) << 8) | l as u16;
                self.push(pc);
                self.pc = word;
            }
            [0x15, 0, 0, 0] => { // RET
                debug!("RET");
                self.pc = self.pop();
            }
            [0x16, x, 0, 0] if x < 16 => { // JMP RX
                let x = x as usize;
                debug!("JMP R{:X}={:04x}", x, self.r[x]);
                self.pc = self.r[x] as u16;
            }
            [0x17, x, l, h] if x < 16 => { // Cx HHLL
                let condition = FromPrimitive::from_u8(x).unwrap();
                let x = x as usize;
                debug!("C{:?} {:02x}{:02x}", condition, h, l);
                if self.check_condition(condition) {
                    let pc = self.pc;
                    self.push(pc);
                    self.pc = self.r[x] as u16;
                }
            }
            [0x18, x, 0, 0] if x < 16 => { // CALL RX
                let x = x as usize;
                debug!("CALL R{:X}={:04x}", x, self.r[x]);
                let pc = self.pc;
                self.push(pc);
                self.pc = self.r[x] as u16;
            }
            [0x20, x, l, h] if x < 16 => { // LDI RX, HHLL
                let x = x as usize;
                debug!("LDI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let word = ((h as i16) << 8) | l as i16;
                self.r[x] = word;
            }
            [0x21, 0, l, h] => { // LDI SP, HHLL
                debug!("LDI SP={:04x}, {:02x}{:02x}", self.sp, h, l);
                let word = ((h as u16) << 8) | l as u16;
                self.sp = word;
            }
            [0x22, x, l, h] if x < 16 => { // LDM RX, HHLL
                let x = x as usize;
                // TODO: Figure out whether this should set flags
                let word = ((h as u16) << 8) | l as u16;
                debug!("LDM R{:X}={:04x}, [{:02x}{:02x}]={:04x}",
                       x, self.r[x], h, l, self.mem.read_word(word));
                self.r[x] = self.mem.read_word(word) as i16;
            }
            [0x23, yx, 0, 0] => { // LDM RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("LDM R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y]);
                self.r[x] = self.mem.read_word(self.r[y] as u16) as i16;
            }
            [0x24, yx, 0, 0] => { // MOV RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("MOV R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                self.r[x] = self.r[y];
            }
            [0x30, x, l, h] if x < 16 => { // STM RX, HHLL
                let x = x as usize;
                debug!("STM R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let word = ((h as u16) << 8) | l as u16;
                self.mem.write_word(word, self.r[x] as u16);
            }
            [0x31, yx, 0, 0] => { // STM RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("STM R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                self.mem.write_word(self.r[y] as u16, self.r[x] as u16);
            }
            [0x40, x, l, h] if x < 16 => { // ADDI RX, HHLL
                let x = x as usize;
                debug!("ADDI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let tx = self.r[x];
                let word = ((h as i16) << 8) | l as i16;
                self.r[x] = self.add(tx, word);
            }
            [0x41, yx, 0, 0] => { // ADD RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("ADD R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.add(tx, ty);
            }
            [0x42, yx, z, 0] if z < 16 => { // ADD RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("ADD R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.add(tx, ty);
            }
            [0x50, x, l, h] if x < 16 => { // SUBI X, HHLL
                let x = x as usize;
                debug!("SUBI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let tx = self.r[x];
                let word = ((h as i16) << 8) | l as i16;
                self.r[x] = self.sub(tx, word);
            }
            [0x51, yx, 0, 0] => { // SUB RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("SUB R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.sub(tx, ty);
            }
            [0x52, yx, z, 0] if z < 16 => { // SUB RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("SUB R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.sub(tx, ty);
            }
            [0x53, x, l, h] if x < 16 => { // CMPI RX, HHLL
                let x = x as usize;
                debug!("CMPI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let tx = self.r[x];
                let word = ((h as i16) << 8) | l as i16;
                self.sub(tx, word);
            }
            [0x54, yx, 0, 0] => { // CMP RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("CMP R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.sub(tx, ty);
            }
            [0x60, x, l, h] if x < 16 => { // ANDI RX, HHLL
                let x = x as usize;
                debug!("ANDI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let tx = self.r[x];
                let word = ((h as i16) << 8) | l as i16;
                self.r[x] = self.and(tx, word);
            }
            [0x61, yx, 0, 0] => { // AND RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("AND R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.and(tx, ty);
            }
            [0x62, yx, z, 0] if z < 16 => { // AND RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("AND R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.and(tx, ty);
            }
            [0x63, x, l, h] if x < 16 => { // TSTI RX, HHLL
                let x = x as usize;
                debug!("TSTI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let tx = self.r[x];
                let word = ((h as i16) << 8) | l as i16;
                self.and(tx, word);
            }
            [0x64, yx, 0, 0] => { // TST RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("TST R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.and(tx, ty);
            }
            [0x70, x, l, h] if x < 16 => { // ORI RX, HHLL
                let x = x as usize;
                debug!("ORI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let tx = self.r[x];
                let word = ((h as i16) << 8) | l as i16;
                self.r[x] = self.or(tx, word);
            }
            [0x71, yx, 0, 0] => { // OR RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("OR R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.or(tx, ty);
            }
            [0x72, yx, z, 0] if z < 16 => { // OR RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("OR R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.or(tx, ty);
            }
            [0x80, x, l, h] if x < 16 => { // XORI RX, HHLL
                let x = x as usize;
                debug!("XORI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let word = ((h as i16) << 8) | l as i16;
                let tx = self.r[x];
                self.r[x] = self.xor(tx, word);
            }
            [0x81, yx, 0, 0] => { // XOR RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("XOR R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.xor(tx, ty);
            }
            [0x82, yx, z, 0] if z < 16 => { // XOR RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("XOR R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.xor(tx, ty);
            }
            [0x90, x, l, h] if x < 16 => { // MULI RX, HHLL
                let x = x as usize;
                debug!("MULI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let word = ((h as i16) << 8) | l as i16;
                let tx = self.r[x];
                self.r[x] = self.mul(tx, word);
            }
            [0x91, yx, 0, 0] => { // MUL RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("MUL R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.mul(tx, ty);
            }
            [0x92, yx, z, 0] if z < 16 => { // MUL RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("MUL R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.mul(tx, ty);
            }
            [0xa0, x, l, h] if x < 16 => { // DIVI RX, HHLL
                let x = x as usize;
                debug!("DIVI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let word = ((h as i16) << 8) | l as i16;
                let tx = self.r[x];
                self.r[x] = self.div(tx, word);
            }
            [0xa1, yx, 0, 0] => { // DIV RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("DIV R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.div(tx, ty);
            }
            [0xa2, yx, z, 0] if z < 16 => { // DIV RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("DIV R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.div(tx, ty);
            }
            [0xa3, x, l, h] if x < 16 => { // MODI RX, HHLL
                let x = x as usize;
                debug!("MODI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let word = ((h as i16) << 8) | l as i16;
                let tx = self.r[x];
                self.r[x] = self.modulo(tx, word);
            }
            [0xa4, yx, 0, 0] => { // MOD RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("MOD R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.modulo(tx, ty);
            }
            [0xa5, yx, z, 0] if z < 16 => { // MOD RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("MOD R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.modulo(tx, ty);
            }
            [0xa6, x, l, h] if x < 16 => { // REMI RX, HHLL
                let x = x as usize;
                debug!("REMI R{:X}={:04x}, {:02x}{:02x}", x, self.r[x], h, l);
                let word = ((h as i16) << 8) | l as i16;
                let tx = self.r[x];
                self.r[x] = self.rem(tx, word);
            }
            [0xa7, yx, 0, 0] => { // REM RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("REM R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.rem(tx, ty);
            }
            [0xa8, yx, z, 0] if z < 16 => { // REM RX, RY, RZ
                let (x, y) = nibbles_from_yx(yx);
                let z = z as usize;
                debug!("REM R{:X}={:04x}, R{:X}={:04x}, R{:X}={:04x}",
                       x, self.r[x], y, self.r[y], z, self.r[z]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[z] = self.rem(tx, ty);
            }
            [0xb0, x, n, 0] if x < 16 && n < 16 => { // SHL RX, N
                let x = x as usize;
                debug!("SHL R{:X}={:04x}, {:x}", x, self.r[x], n);
                let tx = self.r[x];
                self.r[x] = self.shl(tx, n);
            }
            [0xb1, x, n, 0] if x < 16 && n < 16 => { // SHR RX, N
                let x = x as usize;
                debug!("SHR R{:X}={:04x}, {:x}", x, self.r[x], n);
                let tx = self.r[x];
                self.r[x] = self.shr(tx, n);
            }
            [0xb2, x, n, 0] if x < 16 && n < 16 => { // SAR RX, N
                let x = x as usize;
                debug!("SHR R{:X}={:04x}, {:x}", x, self.r[x], n);
                let tx = self.r[x];
                self.r[x] = self.sar(tx, n);
            }
            [0xb3, yx, 0, 0] => { // SHL RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("SHL R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.shl(tx, ty as u8);
            }
            [0xb4, yx, 0, 0] => { // SHR RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("SHR R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.shr(tx, ty as u8);
            }
            [0xb5, yx, 0, 0] => { // SAR RX, RY
                let (x, y) = nibbles_from_yx(yx);
                debug!("SHR R{:X}={:04x}, R{:X}={:04x}", x, self.r[x], y, self.r[y]);
                let (tx, ty) = (self.r[x], self.r[y]);
                self.r[x] = self.sar(tx, ty as u8);
            }
            [0xc0, x, 0, 0] if x < 16 => { // PUSH RX
                let x = x as usize;
                debug!("PUSH R{:X}={:04x}", x, self.r[x]);
                let t = self.r[x];
                self.push(t as u16);
            }
            [0xc1, x, 0, 0] if x < 16 => { // POP RX
                let x = x as usize;
                debug!("POP R{:X}={:04x}", x, self.r[x]);
                self.r[x] = self.pop() as i16;
            }
            [0xc2, 0, 0, 0] => { // PUSHALL
                debug!("PUSHALL");
                for i in 0..16 {
                    let t = self.r[i];
                    self.push(t as u16);
                }
            }
            [0xc3, 0, 0, 0] => { // POPALL
                debug!("POPALL");
                for i in (0..16).rev() {
                    self.r[i] = self.pop() as i16;
                }
            }
            [0xc4, 0, 0, 0] => { // PUSHF
                debug!("PUSHF");
                let Flags(flags) = self.flags;
                self.push(flags as u16);
            }
            [0xd0, 0, l, h] => { // PAL HHLL
                debug!("PAL {:02x}{:02x}", h, l);
                let word = ((h as u16) << 8) | l as u16;
                self.load_palette(word as u16);
            }
            [0xd1, x, 0, 0] if x < 16 => { // PAL RX
                let x = x as usize;
                debug!("PAL R{:X}={:04x}", x, self.r[x]);
                let tx = self.r[x];
                self.load_palette(tx as u16);
            }
            op => panic!("unsupported opcode: {:02x} {:02x} {:02x} {:02x}",
                         op[0], op[1], op[2], op[3])
        }
    }
}
